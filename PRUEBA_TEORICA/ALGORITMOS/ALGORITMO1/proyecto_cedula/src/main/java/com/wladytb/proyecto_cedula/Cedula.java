/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wladytb.proyecto_cedula;

/**
 *
 * @author wladi
 */
public class Cedula {

    //mètodo principal recibe como parametro la identificación
    public String validar(String cedula) {
        String salida = "";
        //valida la identificacion
        switch (cedula.length()) {
            case 10:
                //entra si es cedula
                salida = "CÉDULA" + provincia(cedula);
                break;
            case 13:
                //entra si es ruc
                salida = "RUC" + provincia(cedula);
                break;
            default:
                //si no es cedula ni ruc
                salida = "IDENTIFICACIÓN INVÁLIDA";
                break;
        }
        //retonar el resultado
        return salida;
    }

    public String provincia(String cedula) {
        //verifica si es de guayas
        if (cedula.subSequence(0, 2).equals("09")) {
            return " DE GUAYAS";
        }
        //retorna el resultado
        return " DE OTRA PROVINCIA";
    }

    public static void main(String[] args) {
        Cedula c = new Cedula();
        System.out.println(c.validar("0900000001"));
        System.out.println(c.validar("09001"));
        System.out.println(c.validar("0900000001001"));
        System.out.println(c.validar("09000000000000001"));
        System.out.println(c.validar("1700000001"));
        System.out.println(c.validar("1700000000001"));
        
    }
}
